package it.iwkz.gateway.services;

import it.iwkz.gateway.payloads.ApiResponse;
import it.iwkz.gateway.payloads.AuthUserResponse;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class JwtUserDetailService implements UserDetailsService {
    @Value("${services.alfalah}")
    private String apiUrl;

    private String token;

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Retrofit retrofit = new Retrofit.Builder()
                .client(getClient())
                .baseUrl(apiUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        AuthService authService = retrofit.create(AuthService.class);

        Call<ApiResponse> call = authService.getUser();
        try {
            ApiResponse apiResponse = call.execute().body();
            AuthUserResponse userResponse = apiResponse.getData().get(0);

            if (userResponse.getUsername().equals(username)) {
                List<GrantedAuthority> authorities = userResponse.getRoles().stream().map(role ->
                        new SimpleGrantedAuthority(role.getName())
                ).collect(Collectors.toList());

                return new org.springframework.security.core.userdetails.User(userResponse.getUsername(), "", authorities);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private OkHttpClient getClient() {
        return new OkHttpClient.Builder().addInterceptor(chain -> {
            Request newRequest = chain.request().newBuilder()
                    .addHeader("Authorization", "Bearer " + this.token)
                    .build();
            return chain.proceed(newRequest);
        }).build();
    }
}
