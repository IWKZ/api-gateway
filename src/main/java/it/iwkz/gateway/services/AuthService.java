package it.iwkz.gateway.services;

import it.iwkz.gateway.payloads.ApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AuthService {
    @GET("api/user")
    Call<ApiResponse> getUser();
}