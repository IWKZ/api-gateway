package it.iwkz.gateway.payloads;

import lombok.Data;

import java.util.Set;

@Data
public class AuthUserResponse {
    private String fullName;
    private String username;
    private Set<Role> roles;
}
