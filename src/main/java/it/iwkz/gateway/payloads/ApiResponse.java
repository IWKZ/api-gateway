package it.iwkz.gateway.payloads;

import lombok.Data;

import java.util.List;

@Data
public class ApiResponse {
    private Object message;
    private Object status;
    private List<AuthUserResponse> data;
}
