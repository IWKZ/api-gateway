package it.iwkz.gateway.payloads;

import lombok.Data;

@Data
public class Role {
    private Long id;
    private String name;
}
